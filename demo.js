/*
    Initialize
*/

const canvas = document.getElementById("demo").getContext("2d")
const map = Mapper().createMap(896, 640);

setInterval(() => {
    Mapper().drawMap(canvas, map)
}, 1000 / 60)

/*
    Create
*/

map.outlineSize = 3;
map.outlineColor = "#0f0f0f";
map.gridLineSize = 2;
map.gridColor = "#111111"
map.backgroundColor = "#555555";

/*
    Test object
*/

map.addListener("gridClick", (gridX, gridY) => {
    if(!map.doesObjectExistAtCoords(gridX, gridY))
        if(!map.doesObjectExistAtCoords(gridX + 1, gridY + 1))
            map.addObject(
                Mapper().object(gridX, gridY, 2, 2, {
                    color: "red"
                })
            )
        else{
            console.log("Attempted placing object on already existing object")
        }
    else{
        console.log("Attempted placing object on already existing object")
    }
})