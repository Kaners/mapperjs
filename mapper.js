const Mapper = function() {
    this.object = (x, y, sizeX, sizeY, graphics) => {
        const object = {
            x: x,
            y: y,
            xSize: sizeX,
            ySize: sizeY,
            graphics: graphics || {color: "purple"}
        }

        if(!object.graphics.color)
        object.graphics.color = "purple";

        return object;
    }

    this.drawMap = (canvas, map, done) => {
        if(map.initializeMap){
            map.initializeMap = false;

            canvas.canvas.addEventListener('click', (event) => {
                for(let cb of map.gridClickListeners)
                    cb(Math.floor((event.clientX - canvas.canvas.offsetLeft) / map.gridSize), Math.floor((event.clientY - canvas.canvas.offsetTop) / map.gridSize))
            }, false);
        }

        map.draw(canvas, map);

        if(done)
            done(canvas, map);
    }

    this.Map = function(objSize, xSize, ySize){
        const mapobj = {
            map: [],
            xSize: xSize,
            ySize: ySize,
            objSize: objSize,
            backgroundColor: "gray",
            outlineSize: 0,
            outlineColor: 0,
            gridLineSize: 0,
            gridColor: "black",
            gridSize: 32,
            fullRedraw: true,
            redrawForeground: false,
            redrawBackground: false,
            initializeMap: true,
            gridClickListeners: []
        }

        mapobj.doesObjectExistAtCoords = (x, y) => {
            let exists = false;

            for(let object of map.map){
                for(let i = 0; i < object.xSize; i++){
                    for(let j = 0; j < object.ySize; j++){
                        if((object.x + i) === x && (object.y + j) === y){
                            exists = true;
                            break;
                        }
                    }                    
                }
            }

            return exists;
        }

        mapobj.addListener = (type, callback) => {
            if(type == "gridClick")
                mapobj.gridClickListeners.push(callback);
        }

        mapobj.addObject = (object) => {
            mapobj.map.push(object);
            mapobj.setFullRedraw();
        }

        mapobj.setFullRedraw = () => {
            mapobj.fullRedraw = true;
        }

        mapobj.draw = (canvas, map, fullRedraw) => {
            if(mapobj.fullRedraw || mapobj.redrawForeground || mapobj.redrawBackground){
                let fullredraw = false;
                if(mapobj.fullRedraw){
                    fullredraw = true;
                    canvas.clearRect(0, 0, map.xSize, map.ySize);
                    mapobj.fullRedraw = false;
                }

                if(fullredraw || mapobj.redrawBackground){
                    if(mapobj.outlineSize > 0){
                        // Draw *outline*
                        canvas.fillStyle = mapobj.outlineColor;
                        canvas.fillRect(0, 0, map.xSize, map.ySize);
                        
                        // Draw background over outline
                        canvas.fillStyle = mapobj.backgroundColor;
                        canvas.fillRect(0 + mapobj.outlineSize, 0 + mapobj.outlineSize, map.xSize - (mapobj.outlineSize * 2), map.ySize - (mapobj.outlineSize * 2));
                    }else{
                        canvas.fillStyle = mapobj.backgroundColor;
                        canvas.fillRect(0, 0, map.xSize, map.ySize);
                    }
                }

                if(mapobj.gridLineSize > 0){
                    canvas.lineWidth = mapobj.gridLineSize;

                    let drawX = 0;
                    let drawY = 0;

                    if(fullredraw || mapobj.redrawBackground){
                        canvas.beginPath();
                        while(drawX < mapobj.xSize){
                            drawX += mapobj.gridSize;
                            canvas.strokeStyle = mapobj.gridColor;
                            canvas.moveTo(drawX, 0);
                            canvas.lineTo(drawX, mapobj.ySize);
                        }

                        while(drawY < mapobj.ySize){
                            drawY += mapobj.gridSize;
                            canvas.strokeStyle = mapobj.gridColor;
                            canvas.moveTo(0, drawY);
                            canvas.lineTo(xSize, drawY);
                        }
                        canvas.stroke();
                    }

                    if(fullredraw || mapobj.redrawForeground){
                        for(let obj of mapobj.map){
                            // temp graphics
                            canvas.fillStyle = obj.graphics.color;
        
                            for(let i = 0; i < obj.xSize; i++){
                                for(let j = 0; j < obj.ySize; j++){
                                    canvas.fillRect((obj.x * mapobj.gridSize) + (i * mapobj.gridSize), (obj.y * mapobj.gridSize) + (j * mapobj.gridSize), 32, 32)
                                }
                            }
                        }
                    }
                }
            }
        }

        return mapobj;
    }

    this.createMap = (xSize, ySize) => {
        return Map(32, xSize, ySize);
    }

    return this;
}